from .object import Object


class EntryPointPublisher(Object):

    def __init__(self, name, description, parent):
        Object.__init__(self, name, description, parent)
        self.entry_points = []

    def get_entry_points(self):
        return self._entry_points

    def get_entry_point(self, name):
        for entry_point in self.entry_points:
            if entry_point.get_name() == name:
                return entry_point

    def add_entry_point(self, entry_point):
        self.entry_points.append(entry_point)
