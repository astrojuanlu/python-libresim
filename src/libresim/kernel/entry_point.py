from .object import Object


class EntryPoint(Object):

    def __init__(self, name, description, parent, function=None):
        Object.__init__(self, name, description, parent)
        self._function = function

    def execute(self):
        """This method shall be called when an associated event is emitted."""
        if self._function:
            self._function()
