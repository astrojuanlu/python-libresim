import json

from .field import Field, StructureField


class Publication:

    def __init__(self, simulator):
        self._simulator = simulator
        self._fields = []

    def publish_field(self, field):
        self._fields.append(field)

    def get_field(self, component, name):
        """Get the field of given name."""
        for field in self._fields:
            if field.get_parent() == component:
                if field.get_name() == name:
                    return field

    def get_fields(self):
        """Returns a collection of all fields that have been published."""
        return self._fields

    def store(self, filename):
        vector = {}

        def resolve_field_paths(vector, fields):
            for field in fields:
                path = self._simulator._resolver.get_absolute_path(field)
                if isinstance(field, Field):
                    vector[path] = field.get_value()
                elif isinstance(field, StructureField):
                    vector[path] = {}
                    resolve_field_paths(vector[path], field.get_fields())

        resolve_field_paths(vector, self._fields)

        with open(filename, "w", encoding='utf-8') as f:
            json.dump(vector, f, ensure_ascii=False, indent=4)

    def restore(self, filename):
        with open(filename, "r", encoding='utf-8') as f:
            vector = json.load(f)

        def resolve_field_paths(vector, fields):
            for field in fields:
                path = self._simulator._resolver.get_absolute_path(field)
                if isinstance(field, Field):
                    for key, value in vector.items():
                        if key == path:
                            field.set_value(value)
                elif isinstance(field, StructureField):
                    for key, value in vector.items():
                        if key == path:
                            resolve_field_paths(
                                vector[path], field.get_fields())

        resolve_field_paths(vector, self._fields)
