from .object import Object


class Persist(Object):

    def restore(self, reader):
        """Restore object state from storage."""
        raise NotImplementedError

    def store(self, writer):
        """Store object state to storage."""
        raise NotImplementedError
