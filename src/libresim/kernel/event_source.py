from .object import Object


class EventSource(Object):

    def __init__(self, name, description, parent):
        Object.__init__(self, name, description, parent)
        self._event_sinks = []

    def subscribe(self, event_sink):
        """Subscribe to the event source, i.e. request notifications."""
        self._event_sinks.append(event_sink)

    def unsubscribe(self, event_sink):
        """Unsubscribe from the event source, i.e. cancel notifications."""
        self._event_sinks.pop(event_sink)
