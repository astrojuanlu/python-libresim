from .persist import Persist


class Field(Persist):

    def __init__(self, name, description, parent):
        Persist.__init__(self, name, description, parent)
        self._value = None

    def set_value(self, value):
        self._value = value

    def get_value(self):
        return self._value


class ForcibleField(Field):

    def __init__(self, name, description, parent):
        Field.__init__(self, name, description, parent)
        self._forced_value = None

    def force(self, value):
        """Force the field value so that it does not change until unforced."""
        self._forced_value = value

    def unforce(self):
        self._forced_value = None

    def get_value(self):
        if self._forced_value is not None:
            return self._forced_value
        else:
            return self._value


class StructureField(Persist):

    def __init__(self, name, description, parent):
        Persist.__init__(self, name, description, parent)
        self._fields = []

    def get_fields(self):
        return self._fields

    def get_field(self, name):
        for field in self._fields:
            if field.get_name() == name:
                return field

    def __getattr__(self, name):
        return self.get_field(name)
