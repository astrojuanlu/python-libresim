from .component import Component


class Aggregate(Component):

    def __init__(self, name, description, parent):
        Component.__init__(self, name, description, parent)
        self._references = []

    def get_reference(self, name):
        for reference in self._references:
            if reference.get_name() == name:
                return reference

    def get_references(self):
        return self._references

    def add_reference(self, reference):
        reference._parent = self
        self._references.append(reference)
