import time
import threading
import enum
from datetime import timedelta

import simpy

from .kernel import Composite, Container, Publication
from .kernel.services import Logger, TimeKeeper, EventManager, Scheduler,\
    Resolver, LinkRegistry


class InvalidSimulatorState(Exception):
    pass


class SimulationState(enum.IntEnum):
    BUILDING = 0
    CONNECTING = 1
    INITIALISING = 2
    STANDBY = 3
    EXECUTING = 4
    STORING = 5
    RESTORING = 6
    RECONNECTING = 7
    EXITING = 8
    ABORTING = 9


class SimulationTimeProgress(enum.IntEnum):
    REALTIME = 1
    ACCELERATED = 2
    FREE_RUNNING = 3


class Simulator(Composite):

    def __init__(self, name, description):
        Composite.__init__(self, name, description, None)
        self.add_container(Container("Models", "", self))
        self.add_container(Container("Services", "", self))
        self._logger = Logger(self)
        self._event_manager = EventManager(self)
        self._time_keeper = TimeKeeper(self)
        self._scheduler = Scheduler(self)
        self._resolver = Resolver(self)
        self._link_registry = LinkRegistry(self)
        self._publication = Publication(self)
        self._init_entry_points = []

        self._simulation_time_progress = SimulationTimeProgress.REALTIME
        self._simulation_time_factor = 1
        self._env = None
        self._simulation_thread = None
        self._terminate = False

        self._state = SimulationState.BUILDING

    def publish(self):
        """
        This method asks the simulation environment to call the publish()
        method of all service and model instances in the component
        hierarchy which are still in created state.

        """
        if self._state != SimulationState.BUILDING:
            return

        def recursive_publish(component):
            component._publish(self._publication)
            if isinstance(component, Composite):
                for container in component.get_containers():
                    for component in container.get_components():
                        recursive_publish(component)

        for service in self.get_container("Services").get_components():
            recursive_publish(service)

        for model in self.get_container("Models").get_components():
            recursive_publish(model)

    def configure(self):
        """
        This method asks the simulation environment to call the configure()
        method of all service and model instances which are still in
        publishing state.

        """
        if self._state != SimulationState.BUILDING:
            return

        def recursive_configure(component):
            component._configure(self._logger, self._link_registry)
            if isinstance(component, Composite):
                for container in component.get_containers():
                    for component in container.get_components():
                        recursive_configure(component)

        for service in self.get_container("Services").get_components():
            recursive_configure(service)

        for model in self.get_container("Models").get_components():
            recursive_configure(model)

    def connect(self):
        """
        This method informs the simulation environment that the hierarchy
        of model instances has been configured, and can now be connected to
        the simulator. Thus, the simulation environment calls the connect()
        method of all service and model instances.

        """
        if self._state != SimulationState.BUILDING:
            return

        self._state = SimulationState.CONNECTING

        def recursive_connect(component):
            component._connect(self)
            if isinstance(component, Composite):
                for container in component.get_containers():
                    for component in container.get_components():
                        recursive_connect(component)

        for service in self.get_container("Services").get_components():
            recursive_connect(service)

        for model in self.get_container("Models").get_components():
            recursive_connect(model)

        self.initialise()

    def initialise(self):
        """
        This method asks the simulation environment to call all initialisation
        entry points.

        """
        if self._state == SimulationState.CONNECTING:
            event_id = self._event_manager.query_event_id("LEAVE_CONNECTING")
            self._event_manager.emit(event_id)

        elif self._state == SimulationState.STANDBY:
            event_id = self._event_manager.query_event_id("LEAVE_STANDBY")
            self._event_manager.emit(event_id)

        else:
            return

        self._state = SimulationState.INITIALISING
        event_id = self._event_manager.query_event_id("ENTER_INITIALISING")
        self._event_manager.emit(event_id)

        for init_entry_point in self._init_entry_points:
            init_entry_point()
        self._init_entry_points = []

        event_id = self._event_manager.query_event_id("LEAVE_INITIALISING")
        self._event_manager.emit(event_id)

        self._state = SimulationState.STANDBY
        event_id = self._event_manager.query_event_id("ENTER_STANDBY")
        self._event_manager.emit(event_id)

    def run(self):
        """This method changes from standby to executing state."""

        if self._state != SimulationState.STANDBY:
            return

        event_id = self._event_manager.query_event_id("LEAVE_STANDBY")
        self._event_manager.emit(event_id)

        self._state = SimulationState.EXECUTING
        event_id = self._event_manager.query_event_id("ENTER_EXECUTING")
        self._event_manager.emit(event_id)

        # create the simulation thread
        self._terminate = False
        self._env = self._create_env()
        self._env.process(self._simulation_process())
        self._simulation_thread = threading.Thread(target=self._env.run)
        self._simulation_thread.start()

    def _simulation_process(self):
        while True:

            if self._terminate:
                break

            for event_id, event in sorted(
                    self._scheduler._scheduled_events.items()):

                if self._time_keeper._simulation_time >=\
                        event['simulation_time']:
                    event['entry_point'].execute()

                    # is it a repeating event
                    if event['repeat'] > 0:
                        event['repeat'] -= 1
                        event['simulation_time'] += event['cycle_time']
                    elif event['repeat'] == 0:
                        del self._scheduler._scheduled_events[event_id]
                    else:
                        event['simulation_time'] += event['cycle_time']

            yield self._env.timeout(1)
            self._time_keeper._simulation_time += 1
            self._time_keeper._epoch_time += timedelta(seconds=1)

    def hold(self, immediate=False):
        """This method changes from executing to standby state."""
        if self._state != SimulationState.EXECUTING:
            return

        event_id = self._event_manager.query_event_id("LEAVE_EXECUTING")
        self._event_manager.emit(event_id)

        self._terminate = True
        self._state = SimulationState.STANDBY
        event_id = self._event_manager.query_event_id("ENTER_STANDBY")
        self._event_manager.emit(event_id)

    def store(self, filename):
        """This method is used to store a state vector to file."""
        if self._state != SimulationState.STANDBY:
            return

        event_id = self._event_manager.query_event_id("LEAVE_STANDBY")
        self._event_manager.emit(event_id)

        self._state = SimulationState.STORING
        event_id = self._event_manager.query_event_id("ENTER_STORING")
        self._event_manager.emit(event_id)

        # run external persistance
        self._publication.store(filename)
        # run self persistance
        # (not implemented)

        event_id = self._event_manager.query_event_id("LEAVE_STORING")
        self._event_manager.emit(event_id)

        self._state = SimulationState.STANDBY
        event_id = self._event_manager.query_event_id("ENTER_STANDBY")
        self._event_manager.emit(event_id)

    def restore(self, filename):
        """This method is used to restore a state vector from file."""
        if self._state != SimulationState.STANDBY:
            return

        event_id = self._event_manager.query_event_id("LEAVE_STANDBY")
        self._event_manager.emit(event_id)

        self._state = SimulationState.RESTORING
        event_id = self._event_manager.query_event_id("ENTER_RESTORING")
        self._event_manager.emit(event_id)

        # restoring...
        self._publication.restore(filename)

        event_id = self._event_manager.query_event_id("LEAVE_RESTORING")
        self._event_manager.emit(event_id)

        self._state = SimulationState.STANDBY
        event_id = self._event_manager.query_event_id("ENTER_STANDBY")
        self._event_manager.emit(event_id)

    def reconnect(self, root):
        """
        This method asks the simulation environment to reconnect the
        component hierarchy starting at the given root component.

        """
        raise NotImplementedError

    def exit(self):
        """This method is used for a normal termination of a simulation."""
        self._terminate = True

    def abort(self):
        """This method is used for an abnormal termination of a simulation."""
        raise NotImplementedError

    def get_state(self):
        """Return the current simulator state."""
        return self._state

    def add_init_entry_point(self, entry_point):
        """
        This method can be used to add entry points that shall be executed
        in the initialising state.

        """
        if self._state not in [
                SimulationState.BUILDING,
                SimulationState.CONNECTING,
                SimulationState.STANDBY]:
            return

        self.init_entry_points.append(entry_point)

    def add_model(self, model):
        """
        This method adds a model to the models collection of the simulator,
        i.e. to the "Models" container.

        """
        if self._state not in [
                SimulationState.STANDBY,
                SimulationState.BUILDING,
                SimulationState.CONNECTING,
                SimulationState.INITIALISING]:
            raise InvalidSimulatorState()
        model._parent = self  # simulator becomes parent of contained model
        self.get_container("Models").add_component(model)

    def add_service(self, service):
        """
        This method adds a user-defined service to the services collection,
        i.e. to the "Services" container.

        """
        if self._state not in SimulationState.BUILDING:
            raise InvalidSimulatorState()
        service._parent = self  # simulator becomes parent of contained service
        self.get_container("Services").add_component(service)

    def get_service(self, name):
        raise NotImplementedError

    def get_logger(self):
        return self._logger

    def get_time_keeper(self):
        return self._time_keeper

    def get_scheduler(self):
        return self._scheduler

    def get_event_manager(self):
        return self._event_manager

    def get_resolver(self):
        return self._resolver

    def get_link_registry(self):
        return self._link_registry

    def get_publication(self):
        return self._publication

    def set_time_progress(self, progress, factor=None):
        self._simulation_time_progress = progress
        self._simulation_time_factor = factor

    def _create_env(self):
        if self._simulation_time_progress ==\
                SimulationTimeProgress.REALTIME:
            env = simpy.rt.RealtimeEnvironment(factor=1, strict=False)
        elif self._simulation_time_progress ==\
                SimulationTimeProgress.ACCELERATED:
            env = simpy.rt.RealtimeEnvironment(
                factor=1/self._simulation_time_factor, strict=False)
        elif self._simulation_time_progress ==\
                SimulationTimeProgress.FREE_RUNNING:
            env = simpy.Environment()
        return env
