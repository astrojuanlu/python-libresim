from libresim import EventConsumer

from ..generic_model import GenericModel


class Heater(GenericModel, EventConsumer):

    def __init__(self, name, description, parent):
        GenericModel.__init__(self, name, description, parent)
        EventConsumer.__init__(self, name, description, parent)
