from yaml import safe_load, YAMLError

from libresim import Model, Composite, Field, EntryPoint, Container,\
    EntryPointPublisher
from .hot_object import HotObject
from .thermal_node import RelatedHotObject, ThermalNode


def create_thermal_network(
        name, description, parent,
        delta_time):
    thermal_network = ThermalNetwork(name, description, parent)
    thermal_network.delta_time.set_value(delta_time)
    return thermal_network


class ThermalNetwork(Model, EntryPointPublisher, Composite):

    def __init__(self, name, description, parent):
        Model.__init__(self, name, description, parent)
        Composite.__init__(self, name, description, parent)
        EntryPointPublisher.__init__(self, name, description, parent)

        self.add_container(Container("HotObjects", "", self))
        self.add_container(Container("ThermalNodes", "", self))

        self.delta_time = Field(
            "delta_time", "Delta time (in seconds) between two updates", self)

        self.add_entry_point(EntryPoint(
            "update",
            "Updates temperatures of all thermal nodes in the thermal network",
            self, self.do_update)
        )
        self.update_id = None

    def publish(self):
        self._receiver.publish_field(self.delta_time)

    def configure(self):
        # path the set_value() method, since a change of delta time shall
        # cause a rescheduling of the model
        self.delta_time.set_value = self.set_delta_time

    def connect(self):
        if self.delta_time.get_value() > 0:
            self.update_id = self._scheduler.add_simulation_time_event(
                entry_point=self.get_entry_point("update"),
                simulation_time=0,
                cycle_time=self.delta_time.get_value(),
                repeat=-1)

    def set_delta_time(self, value):
        # remove event if it exists already
        if self.update_id is not None:
            self._scheduler.remove_event(self.update_id)
        # schedule update if delta time not zero
        self.delta_time._value = value
        if self.delta_time.get_value() > 0:
            self.update_id = self._scheduler.add_simulation_time_event(
                entry_point=self.get_entry_point("update"),
                simulation_time=0,
                cycle_time=self.delta_time.get_value(),
                repeat=-1)

    def get_hot_object(self, name):
        for ho in self.get_container("HotObjects").get_components():
            if ho.get_name() == name:
                return ho

    def get_thermal_node(self, name):
        for tn in self.get_container("ThermalNodes").get_components():
            if tn.get_name() == name:
                return tn

    def do_update(self):
        # updates all temperatures of all thermal nodes in the thermal network
        for tn in self.get_container("ThermalNodes").get_components():
            tn.do_update(self.delta_time.get_value())

    @classmethod
    def load_config(cls, filename, name, description, parent, delta_time):
        """Factory method that returns a new instance of ThermalNetwork
        with configuration settings loaded from a yaml file.
        """
        with open(filename) as file:
            try:
                config = safe_load(file)
            except YAMLError as error:
                print(error)

        thermal_network = ThermalNetwork(name, description, parent)
        thermal_network.delta_time.set_value(delta_time)

        hot_objects = thermal_network.get_container("HotObjects")
        thermal_nodes = thermal_network.get_container("ThermalNodes")

        def parse_data(key, value):
            for name, attributes in value.items():
                if attributes is None:
                    attributes = {}

                if key == "hot_objects":
                    description = attributes.get("description", "")
                    ho = HotObject(name, description, hot_objects)
                    status = attributes.get("active", False)
                    ho.status.set_value(status)
                    hot_objects.add_component(ho)
                elif key == "thermal_nodes":
                    description = attributes.get("description", "")
                    tn = ThermalNode(name, description, thermal_nodes)
                    tn.base_temperature.set_value(attributes["base_temperature"])
                    tn.current_temperature.set_value(attributes["current_temperature"])
                    tn.rise_rate.set_value(attributes["rise_rate"])
                    tn.fall_rate.set_value(attributes["fall_rate"])
                    thermal_nodes.add_component(tn)

                    related_hot_objects = tn.get_container("RelatedHotObjects")

                    parse_related_hot_objects(
                        data=attributes["related_hot_objects"],
                        container=related_hot_objects)

        def parse_related_hot_objects(data, container):
            node_name = container.get_parent().get_name()
            for obj_name, max_effect in data.items():
                rho = RelatedHotObject(
                    f"{node_name}_{obj_name}", "", container)
                rho.hot_object.set_value(
                    thermal_network.get_hot_object(obj_name).get_name())
                rho.maximum_effect.set_value(max_effect)
                container.add_component(rho)

        for key, value in config.items():
            parse_data(key, value)
        return thermal_network
