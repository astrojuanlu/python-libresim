from libresim import Model, Composite, Field, Container


def create_thermal_node(
        name, description, parent,
        base_temperature,
        current_temperature,
        rise_rate,
        fall_rate,
        offset,
        scale_factor):
    thermal_node = ThermalNode(name, description, parent)
    thermal_node.base_temperature.set_value(base_temperature)
    thermal_node.current_temperature.set_value(current_temperature)
    thermal_node.rise_rate.set_value(rise_rate)
    thermal_node.fall_rate.set_value(fall_rate)
    return thermal_node


class ThermalNode(Model, Composite):

    def __init__(self, name, description, parent):
        Model.__init__(self, name, description, parent)
        Composite.__init__(self, name, description, parent)

        self.add_container(Container("RelatedHotObjects", "", self))

        self.base_temperature = Field(
            "base_temperature",
            "Base temperature (in C) of thermal node.",
            self)
        self.steady_state_temperature = Field(
            "steady_state_temperature",
            "Steady state temperature (in C) of the thermal node.",
            self)
        self.current_temperature = Field(
            "current_temperature",
            "Current temperature (in C) of thermal node.",
            self)
        self.rise_rate = Field(
            "rise_rate",
            "Temperature rise rate (in C/s) of the thermal node.",
            self)
        self.fall_rate = Field(
            "fall_rate",
            "Temperature fall rate (in C/s) of the thermal node.",
            self)

    def publish(self):
        self._receiver.publish_field(self.base_temperature)
        self._receiver.publish_field(self.steady_state_temperature)
        self._receiver.publish_field(self.current_temperature)
        self._receiver.publish_field(self.rise_rate)
        self._receiver.publish_field(self.fall_rate)

    def configure(self):
        self._thermal_network = self.get_parent().get_parent()

    def do_update(self, delta_time):
        maximum_effect = 0
        for rel_ho in self.get_container("RelatedHotObjects").get_components():
            hot_object = self._thermal_network.get_hot_object(
                rel_ho.hot_object.get_value())
            if hot_object.status.get_value():
                maximum_effect += rel_ho.maximum_effect.get_value()

        self.steady_state_temperature.set_value(
            self.base_temperature.get_value() + maximum_effect)

        if self.current_temperature.get_value() <\
                self.steady_state_temperature.get_value():
            new_temperature =\
                self.current_temperature.get_value()\
                + (self.rise_rate.get_value() * delta_time)
            if new_temperature > self.steady_state_temperature.get_value():
                new_temperature = self.steady_state_temperature.get_value()
        else:
            new_temperature =\
                self.current_temperature.get_value()\
                - (self.fall_rate.get_value() * delta_time)
            if new_temperature < self.steady_state_temperature.get_value():
                new_temperature = self.steady_state_temperature.get_value()

        self.current_temperature.set_value(new_temperature)


def create_related_hot_object(
        name, description, parent,
        hot_object,
        maximum_effect):
    related_hot_object = RelatedHotObject(name, description, parent)
    related_hot_object.hot_object.set_value(hot_object.get_name())
    related_hot_object.maximum_effect.set_value(maximum_effect)
    return related_hot_object


class RelatedHotObject(Model):

    def __init__(self, name, description, parent):
        Model.__init__(self, name, description, parent)
        self.hot_object = Field(
            "hot_object", "Name of the referenced hot object", self)
        self.maximum_effect = Field(
            "maximum_effect", "Contribution (in degC) of the hot object", self)

    def publish(self):
        self._receiver.publish_field(self.hot_object)
        self._receiver.publish_field(self.maximum_effect)
