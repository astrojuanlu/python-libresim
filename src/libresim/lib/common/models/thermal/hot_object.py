from libresim import Model, Field


def create_hot_object(
        name, description, parent,
        status):
    hot_object = HotObject(name, description, parent)
    hot_object.status.set_value(status)
    return hot_object


class HotObject(Model):

    def __init__(self, name, description, parent):
        Model.__init__(self, name, description, parent)

        self.status = Field("status", "Status of the hot object", self)

    def publish(self):
        self._receiver.publish_field(self.status)
