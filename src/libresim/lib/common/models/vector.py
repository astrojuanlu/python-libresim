from libresim import Field, StructureField


class Vector(StructureField):

    def __init__(self, name, description, parent):
        StructureField.__init__(self, name, description, parent)

        self._fields = [
            Field("x", "The x component of vector", self),
            Field("y", "The y component of vector", self),
            Field("z", "The z component of vector", self)
        ]
