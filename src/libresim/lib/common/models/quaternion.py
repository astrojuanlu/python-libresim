from libresim import Field, StructureField


class Quaternion(StructureField):

    def __init__(self, name, description, parent):
        StructureField.__init__(self, name, description, parent)

        self._fields = [
            Field("q1", "The q1 component of quaternion", self),
            Field("q2", "The q2 component of quaternion", self),
            Field("q3", "The q3 component of quaternion", self),
            Field("q4", "The q4 component of quaternion", self)
        ]
