from libresim import Model, Field, EntryPoint
from .quaternion import Quaternion
from .vector import Vector


class CoordinateSystem(Model):

    def __init__(self, name, description, parent):
        Model.__init__(self, name, description, parent)

        self.parent_coordinate_system = Field(
            "parent_coordinate_system", "Parent coordinate system", self)
        self.position = Vector(
            "position", "Position relative to parent coordinate system", self)
        self.rotation = Quaternion(
            "rotation", "Rotation relative to parent coordinate system", self)

        self.ep_do_update = EntryPoint(
            "update",
            "Updates position and rotation of coordinate system wrt to parent",
            self, self.do_update)

    def publish(self):
        self._receiver.publish_field(self.parent_coordinate_system)
        self._receiver.publish_field(self.position)
        self._receiver.publish_field(self.rotation)

    def configure(self):
        self.set_parent_coordinate_system(None)
        self.set_position(0, 0, 0)
        self.set_rotation(0, 0, 0, 1)

    def set_parent_coordinate_system(self, parent_coordinate_system):
        self.parent_coordinate_system.set_value(parent_coordinate_system)

    def get_parent_coordinate_system(self):
        return self.parent_coordinate_system.get_value()

    def set_position(self, x, y, z):
        self.position.get_field("x").set_value(x)
        self.position.get_field("y").set_value(y)
        self.position.get_field("z").set_value(z)

    def get_position(self):
        return (
            self.position.get_field("x").get_value(),
            self.position.get_field("y").get_value(),
            self.position.get_field("z").get_value()
        )

    def set_rotation(self, q1, q2, q3, q4):
        self.rotation.get_field("q1").set_value(q1)
        self.rotation.get_field("q2").set_value(q2)
        self.rotation.get_field("q3").set_value(q3)
        self.rotation.get_field("q4").set_value(q4)

    def get_matrix_to(self, coordinate_system):
        raise NotImplementedError

    def get_matrix_from(self, coordinate_system):
        raise NotImplementedError

    def do_update(self):
        # child classes to implement update of position and rotation
        raise NotImplementedError
