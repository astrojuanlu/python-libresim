import enum

from libresim import Model, Composite, Container, Field


class ElementEvent(enum.IntEnum):
    ON_LOAD_CHANGED = 1
    ON_POWERED = 2
    ON_NOT_POWERED = 3


class ElectricalNetwork(Model, Composite):

    def __init__(self, name, description, parent):
        super().__init__(name, description, parent)

        self._delta_time = None
        self._update_event_id = None

        # self._update_entrypoint = EntryPoint(
        #     "Update",
        #     "Updates temperatures of all thermal nodes in the thermal network.",
        #     self,
        #     self.update
        #     )

        self._containers = []

    def publish(self):
        pass#self._receiver.publish_field(self.delta_time)

    def get_total_load(self):
        pass

    def validate(self):
        pass


class Element(Model, Composite):

    def __init__(self, name, description, parent):
        Model.__init__(self, name, description, parent)

        self.add_container(Elements('Elements', "", self))
        self.add_container(Connections('Connections', "", self))

        self.is_top = Field('is_top', "Indicates if element is top most element of electrical network", self)
        self.load = Field('load', "Intrinsic load of the element", self)
        self.total_load = Field('total_load', "Sum of intrinsic load of the element and all other elements connected further down", self)
        self.powered = Field('powered', "Power status of the element", self)
        input_voltage = None
        output_voltage = None

        # event sources
        self.on_load_changed = libresim.EventSource("OnLoadChanged", "", self)

    def do_update(self):
        pass

    def connect_elements(self, lower, upper):
        pass


class Elements(Model, Container):

    def __init__(self, name, description, parent):
        Model.__init__(self, name, description, parent)
        Container.__init__(self, name, description, parent)


class Connection(Model):

    def __init__(self, name, description, parent):
        Model.__init__(self, name, description, parent)
        Container.__init__(self, name, description, parent)


class Connections(Model, Container):

    def __init__(self, name, description, parent):
        Model.__init__(self, name, description, parent)
        Container.__init__(self, name, description, parent)


class SimplePole(Element):

    def __init__(self, name, description, parent):
        Element.__init__(self, name, description, parent)

        self.position = Field('position', "Position of pole", self)


class PowerSource(Element):
    pass


class Terminator(Element):
    pass


# class Action(libresim.Component, libresim.Aggregate, libresim.EventConsumer):
#
#     def __init__(self, name, description, parent):
#         super().__init__(name, description, parent)
#
#         self._action_status = None
#         self._locking_status = None
#
#         self._references = []
#
#
#
# element.on_load_changed.subscribe(action.execute)
