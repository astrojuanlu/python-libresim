from libresim import Model, EntryPoint, EntryPointPublisher


class PrintTimes(Model, EntryPointPublisher):

    def __init__(self, name, description, parent):
        Model.__init__(self, name, description, parent)

        self.add_entry_point(EntryPoint(
            "print", "", self, function=self.do_print_times))

    def connect(self):
        self._scheduler.add_simulation_time_event(
            self.get_entry_point("print"),
            simulation_time=0, cycle_time=1, repeat=-1)

    def do_print_times(self):
        epoch_time = self._time_keeper.get_epoch_time()
        mission_start_time = self._time_keeper.get_mission_start_time()
        mission_time = self._time_keeper.get_mission_time()
        simulation_time = self._time_keeper.get_simulation_time()
        zulu_time = self._time_keeper.get_zulu_time()

        print(
            f"Epoch: {epoch_time}\n"
            f"Mission Start Time: {mission_start_time}\n"
            f"Mission Time: {mission_time}\n"
            f"Simulation Time: {simulation_time}\n"
            f"Zulu Time: {zulu_time}\n"
            "\n")


root = [PrintTimes("PrintTimes", "Print the different time kinds", None)]
