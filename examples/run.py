import argparse
import importlib

import libresim

# load model from external file
parser = argparse.ArgumentParser()
parser.add_argument('modelfile')
args = parser.parse_args()

# allow loading modules from subfolders
modelfile = args.modelfile
modelfile = modelfile.replace("/", ".")  # Linux
modelfile = modelfile.replace("\\", ".")  # Windows
# strip away file ennding if present
if modelfile.endswith(".py"):
    modelfile = modelfile.strip(".py")

module = importlib.import_module(modelfile)

# create simulator
simulator = libresim.Simulator("MySim", "")
simulator.set_time_progress(libresim.SimulationTimeProgress.REALTIME)
# simulator.set_time_progress(libresim.SimulationTimeProgress.ACCELERATED, 10)
# simulator.set_time_progress(libresim.SimulationTimeProgress.FREE_RUNNING)

# add models
for component in module.root:
    if isinstance(component, libresim.Model):
        simulator.add_model(component)
    elif isinstance(component, libresim.Service):
        simulator.add_service(component)
print()
print("Model tree:")


def traverse_model_tree(element, indent):
    print("  ╚" + indent * "═" + f" {element.get_name()}")
    if isinstance(element, libresim.Container):
        for component in element.get_components():
            traverse_model_tree(component, indent+1)
    elif isinstance(element, libresim.Composite):
        for container in element.get_containers():
            print("  ╚" + (indent + 1) * "═" + f" {container.get_name()}")
            for component in container.get_components():
                traverse_model_tree(component, indent+2)


traverse_model_tree(simulator.get_container("Models"), 0)
print()

simulator.publish()
print("Published fields:")
for field in simulator.get_publication().get_fields():
    print(f" * {field.get_parent().get_name()}.{field.get_name()}: {field.get_description()}")
print()
simulator.configure()
simulator.connect()

# simulator.restore("dummy.state")

print("Simulation running...")
print("Press <Enter> to stop")
print()
simulator.run()
input()
simulator.hold()

# simulator.store("dummy.state")

simulator.exit()
