from libresim import Model, Field, EntryPoint, EntryPointPublisher


class Counter(Model, EntryPointPublisher):

    def __init__(self, name, description, parent):
        Model.__init__(self, name, description, parent)
        EntryPointPublisher.__init__(self, name, description, parent)

        self.count = Field("count", "Holds the counter value", self)

        self.add_entry_point(EntryPoint(
            "increase", "", self, function=self.do_count))
        self.add_entry_point(EntryPoint(
            "reset", "", self, function=self.do_reset))

    def publish(self):
        self._receiver.publish_field(self.count)

    def configure(self):
        self.count.set_value(0)

    def connect(self):
        self._scheduler.add_simulation_time_event(
            self.get_entry_point("increase"),
            simulation_time=0, cycle_time=1, repeat=-1)
        self._scheduler.add_simulation_time_event(
            self.get_entry_point("reset"),
            simulation_time=4)
        self._logger.log_debug(self, "Counter Model is now connected")

    def do_reset(self):
        self.count.set_value(0)

    def do_count(self):
        self._logger.log_info(self, f"count is now {self.count.get_value()}")
        self.count.set_value(self.count.get_value() + 1)


root = [Counter("Counter", "A simple counter", None)]
