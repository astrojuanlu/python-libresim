from libresim import EventProvider, EventConsumer, EventSink, EventSource,\
    EntryPoint, Model


class Ticker(Model, EventProvider):

    def __init__(self, name, description, parent):
        Model.__init__(self, name, description, parent)
        EventProvider.__init__(self, name, description, parent)

        self.add_event_source(EventSource("tick", "", self))
        self.ep_do_tick = EntryPoint("Tick", "", self, function=self.do_tick)

    def connect(self):
        self._scheduler.add_simulation_time_event(
            self.ep_do_tick, simulation_time=0, cycle_time=1, repeat=-1)

    def do_tick(self):
        print("tick")
        event_source = self.get_event_source("tick")
        for event_sink in event_source._event_sinks:
            event_sink.notify(self)


class Counter(Model, EventConsumer):

    def __init__(self, name, description, parent):
        Model.__init__(self, name, description, parent)
        EventConsumer.__init__(self, name, description, parent)

        self.add_event_sink(Count("count", "", self))


class Count(EventSink):

    def notify(self, sender, arg=None):
        print("tock")


ticker = Ticker("ticker", "", None)
counter1 = Counter("counter1", "", None)
counter2 = Counter("counter2", "", None)

ticker.get_event_source("tick").subscribe(counter1.get_event_sink("count"))
ticker.get_event_source("tick").subscribe(counter2.get_event_sink("count"))

root = [ticker, counter1, counter2]
