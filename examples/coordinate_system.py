from libresim import Model, EntryPoint
from libresim.lib.common.models.coordinate_system import CoordinateSystem


class MyCoordinateSystem2(CoordinateSystem):

    def do_update(self):
        self.position.x.set_value(self.position.x.get_value() + 1)


coordsys1 = CoordinateSystem("CoordSys1", "", None)
coordsys2 = MyCoordinateSystem2("CoordSys2", "", None)


class Example(Model):

    def __init__(self, name, description, parent):
        Model.__init__(self, name, description, parent)
        self.ep_print_fields = EntryPoint(
            "", "", self, function=self.print_fields)

    def connect(self):
        coordsys2 = self._resolver.resolve_absolute("Models/CoordSys2")
        self._scheduler.add_simulation_time_event(
            coordsys2.ep_do_update, simulation_time=0, cycle_time=1, repeat=-1)

        self._scheduler.add_simulation_time_event(
            self.ep_print_fields, simulation_time=0, cycle_time=1, repeat=-1)

    def print_fields(self):
        coordsys2 = self._resolver.resolve_absolute("Models/CoordSys2")
        print(
            coordsys2.get_name(),
            coordsys2.position.x.get_value(),
            coordsys2.position.y.get_value(),
            coordsys2.position.z.get_value(),
            )


root = [coordsys1, coordsys2, Example("Example", "", None)]
