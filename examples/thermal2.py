"""
This example demonstrates the loading of thermal network models from file.

"""

from libresim import Model, EntryPoint
from libresim.lib.common.models.thermal import ThermalNetwork


# Load from config file:
thermal_network = ThermalNetwork.load_config(
    "thermal_config.yaml", "ThermalNetwork", "", None,
    delta_time=1)


class ExampleModel(Model):

    def __init__(self, name, description, parent):
        Model.__init__(self, name, description, parent)
        self.ep_print_fields = EntryPoint(
            "", "", self, function=self.print_fields)

    def connect(self):
        self._scheduler.add_simulation_time_event(
            self.ep_print_fields,
            simulation_time=0, cycle_time=1, repeat=-1)

    def print_fields(self):
        thermal_nodes = self._resolver.resolve_absolute(
            "Models/ThermalNetwork/ThermalNodes")
        for tn in thermal_nodes.get_components():
            print(f"{tn.get_name()}: {tn.current_temperature.get_value()}")


root = [thermal_network, ExampleModel("ExampleModel", "", None)]
