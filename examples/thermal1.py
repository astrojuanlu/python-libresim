"""
This example demonstrates the manual creation of thermal network models.

"""

from libresim import Model, Container, Composite, EntryPoint
from libresim.lib.common.models.thermal import create_thermal_network,\
    create_hot_object, create_thermal_node, create_related_hot_object


class ExampleModel(Model, Composite):

    def __init__(self, name, description, parent):
        Model.__init__(self, name, description, parent)
        Composite.__init__(self, name, description, parent)

        #######################################################################

        thermal_network = create_thermal_network(
            "ThermalNetwork", "", self,
            delta_time=1)

        heaters = thermal_network.get_container("HotObjects")

        heater1 = create_hot_object(
            "Heater1", "", heaters,
            status=True)
        heaters.add_component(heater1)

        thermistors = thermal_network.get_container("ThermalNodes")

        thermistor1 = create_thermal_node(
            "Thermistor1", "", thermistors,
            base_temperature=20,
            current_temperature=25,
            rise_rate=1,
            fall_rate=1,
            offset=0,
            scale_factor=1)
        thermistors.add_component(thermistor1)

        related_hot_objects = thermistor1.get_container("RelatedHotObjects")

        thermistor1heater1 = create_related_hot_object(
            "Thermistor1_Heater1", "", related_hot_objects,
            heater1,
            20)
        related_hot_objects.add_component(thermistor1heater1)

        #######################################################################

        self.add_container(Container("Submodels", "", self))
        self.get_container("Submodels").add_component(thermal_network)
        self.thermal_nodes = thermal_network.get_container("ThermalNodes")

        self.ep_print_fields = EntryPoint(
            "", "", self, function=self.print_fields)

    def connect(self):
        self._scheduler.add_simulation_time_event(
            self.ep_print_fields,
            simulation_time=0, cycle_time=1, repeat=-1)

    def print_fields(self):
        for thermal_node in self.thermal_nodes.get_components():
            print(f"{thermal_node.get_name()}: {thermal_node.current_temperature.get_value()}")


root = [ExampleModel("ExampleModel", "", None)]
