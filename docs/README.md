# LibreSim

LibreSim is a simulator that follows the ECSS Standard [ECSS-E-ST-40-07C](ECSS-E-ST-40-07C.pdf).

ECSS-E-ST-40-07 is a standard published by the [European Cooperation for Space
Standardization](https://ecss.nl/). It is specific to simulation software and
covers both simulation environments and simulation models. The standard enables
the effective reuse of simulation models within and between space projects and
their stakeholders.

The objective of LibreSim is to provide a simulation infrastructure that can
be extended with custom (mission-specific) models to simulate various kinds of missions.

The main purpose of any simulator is to model the actions and reactions of the
real system during operations. The simulator is used during mission preparation
for training, procedure validations and ground segment testing, and during mission
operations to test proposed changes to the system before they are deployed to the
real vehicle.

The simulator interfaces with the mission control system through telecommand
and telemetry links. The simulator is controlled through command line or via
a GUI application (both to be developed).

## Architecture

The simulator architecture consists of:
- a simulation environment that provides base classes and simulation services
- generic models that provide the basis for simulations
- generic units as basis for modeling the user specific system to be simulated

![](figures/architecture.png)

The simulation services comprise:

- Logger: Allows any component to log messages
- Scheduler*: Allows calls of entry points based on timed or cyclic events
- Time Keeper*: Provides the four different time kinds
- Event Manager*: Provides mechanisms for global asynchronous events
- Resolver*: Provides the ability to get a reference to any model within a simulation
- Link Registry: Maintains a list of the links between model instances

All information regarding the simulation environment and the basic simulation
models can be found in the ECSS-E-ST-40-07 standard.

### Scheduler

The Scheduler service has the purpose to execute scheduled events. The scheduler
maintains the scheduled events in the from of a dictionary with {key:event}, where
key is an increasing event number and event contains the information about the
event (a reference to the entrypoint, the simulation time to execute the event, etc.)




### TimeKeeper

The TimeKeeper service manages the four different time kinds.

- **Simulation Time**: Relative time since start of simulation. Progresses when simulator is running.

  Example: 0...1...2

- **Zulu Time**: The computer clock time, that is, the current wall clock time.

  Example: 2022-05-13T10:08:11...2022-05-13T10:08:12...2022-05-13T10:08:13 (for real-time progress)

- **Epoch Time**: The calendar time of the simulation. Typically set to months or years in the future.

  Example: 2030-01-01T00:00:00...2030-01-01T00:00:01...2030-01-01T00:00:02

- **Mission Start Time**: A calender time, typical the launch of the mission.

  Example: 2020-08-01T10:08:11

- **Mission Time**: A relative time, calculated as EpochTime - MissionStartTime.

  Example: 327682...327683...327684

Epoch and mission time have fixed offset to simulation time and progress linear with simulation time.






## Logical Structure

### Simulator Class

### Object Class

Serves as base class for almost all other classes. It provides name, description
and parent, which have to be defined at construction time.

### Collection Class

Serves as a generic collection. The collection allows to Add and Remove children,
and to Clear its content. Typically, the Add operation does check for a unique name.
For a few cases where this is not required, the AddNonUnique operation is provided.

### Component Class

Component is the base class for all models and services. It provides state and
state transition methods.

### Model Class

An empty class, derived from Component, to server as basis for all models.

### Service Class

An empty class, derived from Component, to server as basis for all services.


## Generic Models

The generic models comprise a suite of generic simulation models which provide a
basis for other, more specialized models.

The generic models consists of following packages:
- thermal: simplified models for heat generation and temperature measurement
- electrical: models electrical power distribution networks
- orbit: models for spacecraft trajectories and the space environment
- payload: generic components to build payload models
- dynamics: rigid body models for spacecraft attitude and rotations
- datalink: provides functionalities to encode/decode frames and packets
- spacelink: modelling of the radio frequency communication between ground and spacecraft

![](assets/architecture.png)

### Thermal

The thermal module allows to assembly a simplified thermal simulation model. It
consists of Hot Objects and Thermal Nodes. A thermal node can have a number of
related hot objects that influence its temperature. Typically hot objects
represent heaters (or any kind of powered unit that produces heat) and thermal nodes
represent thermistors that measure temperature. Hot objects can be either on or off. The status can be set.

![](assets/generic_model_thermal_architecture.png)

Thermal nodes maintain a current temperature, which goes towards its base
temperature (with a rate given by its fall rate) when the related hot objects
are off. If related hot objects are on, the target temperature is the base
temperature plus the contributions of the active hot objects.

Temperatures are cyclically computed in correspondence to each Thermal Node according to the following algorithm: at each step a steady state temperature is computed as sum of a base temperature, typical of the node, and the temperature contribution of each Related Hot Object whose status is on. If the computed steady state temperature is over the temperature of the previous step then the node temperature is increased with a specified rate, otherwise if the computed steady state temperature is under the temperature of the previous step then the node temperature is decreased with a specified rate.


### Electrical

The electrical module allows to assembly a network of elements to model the
distribution of power within an electrical network. The main elements in such
network are nodes and poles, both derived from a basic element model. Nodes are
mainly characterized by their intrinsic load, whereas poles are characterized
by their switch position.

The following nodes shall be modelled:
- power source: supplies the network and most be the top element
- power node: a point or junction in the network
- power bus: distributes power through a network
- terminator: end point of a branch in the network

The following poles shall be modelled:
- simple pole: can be either open or closed
- fuse: derived from simple pole; it opens when load exceeds a threshold
- multi-throw pole: a pole with a number of switch positions
- transfer switch: the reverse of a multi-throw; selects between power supplies

The electrical network is a collection of those nodes and poles with connections
between those elements. For each element, a number of conditions are defined
which can trigger actions. An action can be used, triggered, or called from other simulation models.

### Orbit

The orbit module provides means to model orbital/trajectory movement of an object
in the solar system and includes the calculation of planet positions, perturbation
effects on the orbit. It computes the position and velocity of the object,
taking into account the forces applied. It also defines a number of generic
coordinate systems, including one for each planet and a local one for the
object (spacecraft) to be modelled.

The following aspects shall be modelled:
- orbit propagator
- celestial body (planets)
- earth magnetic field
- earth atmosphere
- earth albedo
- solar flux
- eclipse
- air drag perturbation
- solar pressure perturbation
- gravity field perturbation
- external perturbation

### Payload

The payload modules provides generic models to allow modelling of typical
packet-based payloads (instruments). It consists of four major components:

- interface to data handling system: deals with payload specific communication protocol, to extract telecommand packets and send out telemetry packets
- payload functional model: implements specific behaviour of payload
- packet encoder: encodes telemetry packets, using data received from functional model
- packet decoder: decodes telecommand packets and forwards to functional model for execution

## Generic Units
